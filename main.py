import datetime

#Song File with various attributes of the object song

TitleOfSong = "Mambo Dhuterere Ahuwerere" 					#The title of the song
Artist = "Mambo Dhuterere" 									#The name of the person who sang the song
AlbumTitle = "Dare Guru"  									#Title of the album
TrackNumber = 5 												#Track number on the album
YearOfRelease = datetime.datetime(2019, 8, 27)		#The date the song was released
Genre = "Gospel" 											#Genre of the song
Rating = 4.8 												#Rating of the song
IsHitSong= True											#Whether the song is a hit song
RecordingStudioName = "Mt Zion Music"									#The name of Studio that recorded the song
NumberOfDownloads = 788789 									#Number of times the song was downloaded
SongGrade = 'A' 
DurationOfSeconds = 7896

#/Print each attribute of the Song object
print(TitleOfSong)
print(Artist)
print(AlbumTitle)
print(TrackNumber)
print(YearOfRelease)
print(Genre)
print(Rating)
print(IsHitSong)
print(RecordingStudioName)
print(NumberOfDownloads)
print(SongGrade)
print(DurationOfSeconds)